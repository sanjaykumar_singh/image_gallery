/**
 * Application main controller
 **/
app.controller('mainCtrl', function ($rootScope, $scope, appService, modalService, appFactory) {
    $scope.loader = true;
    $rootScope.countloaded = 0;
    $scope.photoList = [];

    /**
     * Call the appServoce loadData method to make the $q service call
     **/
    appService.loadData().then(function (result) {
        // Set the album list
        modalService.setAlbumList(result[0].data);
        $scope.albumList = modalService.getAlbumList();
        // Set the photo list
        modalService.setPhotoList(result[1].data);
        $scope.loader = false;
    });

    /**
     * Handle the click event of Album
     **/
    $scope.onAlbumClick = function (item) {
        $rootScope.countloaded = 0;
        $scope.loader = true;
        // Getting the album photos based on the item
        $scope.photoList = appFactory.getAlbumPhotos(item);
    }

    /**
     * Handle the click event of photo
     **/
    $scope.onPhotoClick = function (item) {
        $scope.photoObj = item;
        // Show the modal popup
        $('#photoModal').modal('show');
    }

    /**
     * Handle the click event of Back button
     **/
    $scope.onBackBtnClick = function () {
        $scope.photoList = [];
        $scope.loader = false;
    }
});