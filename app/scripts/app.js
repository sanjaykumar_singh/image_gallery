var app = angular.module('mainApp', []);

/**
 * loader directive which has show/hide function call on loader scope variable change watcher.
 **/
app.directive('loader', function () {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="parent-container"><div class="child-container"><img src="assets/images/loader.gif" width="30" height="30"/>  LOADING...</div></div>',
        link: function (scope, element, attr) {
            scope.$watch('loader', function (val) {
                if (val)
                    $(element).show();
                else
                    $(element).hide();
            });
        }
    }
});

/**
 * albumlist directive which will load the albumlisttemplate.html template to show the album tilelist.
 **/
app.directive('albumlist', function (appFactory) {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/views/templates/albumlisttemplate.html'
    }
});

/**
 * photolist directive which will load the photolisttemplate.html template to show the photo tilelist.
 **/
app.directive('photolist', function (appFactory) {
    return {
        restrict: 'E',
        templateUrl: 'app/views/templates/photolisttemplate.html'
    }
});

/**
 * photoload directive which will hide the loader once all the album photos loaded.
 **/
app.directive('photoload', function ($rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.bind('load', function () {
                $rootScope.countloaded++;
                if (scope.countloaded == scope.photoList.length - 1) {
                    scope.loader = false;
                    angular.element('.parent-container').hide();
                }
            });
        }
    };
});