/**
 * appService which will call the multiple service request using $q
 **/
app.service('appService', ['$http', '$q', function ($http, $q) {
    return {
        loadData: function () {
            return $q.all([
         $http.get('http://jsonplaceholder.typicode.com/albums/'),
         $http.get('http://jsonplaceholder.typicode.com/photos')
      ]);
        }
    }
}]);

/**
 * modalService which will hold the app data
 **/
app.service('modalService', function () {
    var albumList = [];
    var photoList = [];
    // Getter AlbumList
    this.getAlbumList = function () {
        return albumList;
    }
    // Setter AlbumList
    this.setAlbumList = function (list) {
        albumList = [];
        albumList = list;
    }
    // Getter PhotoList
    this.getPhotoList = function () {
        return photoList;
    }
    // Setter PhotoList
    this.setPhotoList = function (list) {
        photoList = [];
        photoList = list;
    }
});

/**
 * appFactory which will filter the photolist based on the album selected
 **/
app.factory('appFactory', function (modalService) {
    return {
        getAlbumPhotos: function (item) {
            var photoList = [];
            var list = modalService.getPhotoList();
            for (var i = 0; i < list.length; i++) {
                if (item.id == list[i].albumId) {
                    photoList.push(list[i]);
                }
            }
            return photoList;
        }
    }
});